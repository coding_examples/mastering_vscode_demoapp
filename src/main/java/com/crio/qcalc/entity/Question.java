package com.crio.qcalc.entity;

import org.springframework.data.annotation.Id;

public class Question {
    @Id
    public int questionId;
    public String questionText;
    public Answer answer;
}
