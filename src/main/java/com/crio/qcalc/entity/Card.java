package com.crio.qcalc.entity;

import java.util.List;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Card {
    @Id
    public int cardId;
    public String cardName;
    public List<Question> questions;
}
