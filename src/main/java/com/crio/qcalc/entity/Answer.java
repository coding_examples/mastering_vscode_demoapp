package com.crio.qcalc.entity;

import org.springframework.data.annotation.Id;

public class Answer {
    @Id
    public int answerId;
    public String answerText;
}
