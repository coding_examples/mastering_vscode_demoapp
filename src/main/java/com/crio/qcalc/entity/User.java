package com.crio.qcalc.entity;

import org.springframework.data.annotation.Id;

public class User {
    @Id
    public int userId;
    public String name;
}
