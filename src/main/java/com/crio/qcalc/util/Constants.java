package com.crio.qcalc.util;

public final class Constants {

    public static enum ENV {
        DEV,
        PROD
    }
    public static final String REPO_URL = "mydemoapp.com";
    
}