package com.crio.qcalc.repository.question;

import org.springframework.data.mongodb.repository.MongoRepository;
import com.crio.qcalc.entity.Question;


public interface QuestionRepository extends MongoRepository<Question, Integer> {
    public Question findById(int questionId);
    public void deleteByQuestionId(int questionId);
}