package com.crio.qcalc.repository.card;

import com.crio.qcalc.entity.Card;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface CardRepository extends MongoRepository<Card, Integer> {

    public Card findById(int cardId);
    public void deleteByCardId(int cardId);
    
}