package com.crio.qcalc.repository.user;

import com.crio.qcalc.entity.User;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, Integer> {

    public User findById(int userId);
}