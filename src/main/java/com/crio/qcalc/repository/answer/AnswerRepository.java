package com.crio.qcalc.repository.answer;

import com.crio.qcalc.entity.Answer;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface AnswerRepository extends MongoRepository<Answer, Integer> {

    public Answer findById(int answerId);
    
}