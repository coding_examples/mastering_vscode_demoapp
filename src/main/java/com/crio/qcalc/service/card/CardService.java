package com.crio.qcalc.service.card;

import java.util.List;

import com.crio.qcalc.entity.Card;
import com.crio.qcalc.entity.Question;
import com.crio.qcalc.repository.card.CardRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CardService {

    @Autowired
    private CardRepository cardRepository;

    public Card insertCard(List<Question> questions) {
        Card card = new Card();
        card.setQuestions(questions);

        return cardRepository.insert(card);
    }

    public Card updateCard(int cardId, List<Question> questions) {
        // TODO: comment out older version of this method
        Card card = cardRepository.findById(cardId);

        if (card == null) {
            card = insertCard(questions);
        } else {
            card.setQuestions(questions);
            card = cardRepository.save(card);
        }

        return card;
    }

    public Card updateCardById(int cardId, List<Question> questions) {
        Card card = cardRepository.findById(cardId);
        card.setQuestions(questions);
        return cardRepository.save(card);
    }

    public boolean deleteCard(Card card) {
        // TODO: to be implemented
        return false;
    }

    public boolean deleteCardById(int card) {
        // TODO: to be implemented
        return false;
    }

    public Card getCardByQuestion(Question question) {
        // TODO : to be implemented
        return null;
    }

}